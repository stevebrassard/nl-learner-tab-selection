const path = require('path');
const webpack = require("webpack");

module.exports = {
	entry: './src/app.js',
	output: {
        //Webpack Output Bundle File
        path: path.join(__dirname, 'public/scripts'),
        filename: 'bundle.js'
        //,publicPath: '/public'
	},    
    module: {
        rules: [            
        {
            loader: 'html-loader',
            test: /\.html$/
        },    
        {
            //Babel Loader
            loader: 'babel-loader',
            test: /\.js$/,
            exclude: /node_modules/
        }, 
        { 
            //CSS Loader
            loader: 'style-loader!css-loader',
            test: /\.css$/ 
        },
        { 
            //File Loader
            loader: 'file-loader',
            test: /\.(png|jpg|gif)$/
        },
        {
            //SASS Loader
            test: /\.scss$/,
            use: [
                "style-loader",
                "css-loader",
                "sass-loader"
            ]
        }      
    ]}, 
    plugins: [
        //Minify and Compress Bundled Output
        new webpack.optimize.UglifyJsPlugin({sourceMap: true, minimize: true, compress: { warnings: false }})
    ],
    devtool: 'cheap-module-eval-source-map'  
};
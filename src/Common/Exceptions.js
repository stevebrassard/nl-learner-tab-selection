export class DatasourceException {
    constructor(message) {
        this.message = message;
        this.name = 'DatasourceException';
    }
}
import ko from 'knockout';
import PageViewModel from './ViewModels/PageViewModel';
import LearnerTabSelection from './Components/LearnerTabSelectionComponent';

ko.applyBindings(new PageViewModel(), document.getElementById('root')); //represents some VM the componets belong to
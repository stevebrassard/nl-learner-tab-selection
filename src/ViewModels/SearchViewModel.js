import * as ko from 'knockout';
import SearchViewModelBase from '../ViewModels/SearchViewModelBase';

export default class SearchViewModel extends SearchViewModelBase {
    constructor(props) {
        super(props);
        this.page = 1;
        this.cancelScrollRequest = false;
        this.searchText.subscribe((function(value, args) { 
            this.page = 1; //reset
            this.cancelScrollRequest = false; //reset
        }).bind(this));           
    }

    scrolled(data, event) {        
        let elem = $(event.target);
        if (elem.scrollTop() > (elem[0].scrollHeight - elem[0].offsetHeight - 40)) { 
            this.page++;
            var ajaxOptions = {
                url: this.urlBuilder(this.props.datasource.remote.url, this.searchText(), this.page, this.props.datasource.pageSize, null, 'asc'),
            };    
            if(!this.cancelScrollRequest) {               
                this.fetch(ajaxOptions, true).done( (response) => {
                    if(response.length == 0) {
                        this.cancelScrollRequest = true;
                    } else {
                        let resultData = this.getMappedData(response);
                        this.results(this.results().concat(resultData));                     
                    }
                });
            }
        }   
    }    
}
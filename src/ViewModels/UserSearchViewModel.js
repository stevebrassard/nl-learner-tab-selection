import ko from 'knockout';
import postbox from 'knockout-postbox';
import SearchViewModel from '../ViewModels/SearchViewModel';
import {debounce} from '../Common/Utilities';
import CustomUser from '../ViewModels/CustomUser';

export default class UserSearchViewModel extends SearchViewModel {
    constructor(props) {
        super(props);
        this.props = props;
        this.scrolled = debounce(this.scrolled, 200);

        //listen for text input changes and execute search
        this.searchText.subscribe((function(query) { 
           this.executeSearch(query).done(function(response) {
                let resultData = this.getMappedData(response);
                this.results(resultData);     
            });   
        }).bind(this));

        //execute default fetch (empty string) to pull all data from service 
        this.fetch({ url: this.urlBuilder(this.props.datasource.remote.url, "", 1, this.props.datasource.pageSize, null, 'asc')})
            .done(function(response) {
                let resultData = this.getMappedData(response);
                this.results(resultData);     
        });
    }

    onRemoveUser() {
        console.log("UsersResultsViewViewModel->onRemoveUser()", this);
    }

    displaySettings() {
        console.log("UsersResultsViewViewModel->displaySettings()", this);
    }

    sayGoodbye() {
        alert("goodbye!");
    }

    //Override Base Implementation
    getMappedData(response) {
        return response.map( (data, key) =>  { 
            return new CustomUser(data);
        });
    }

}
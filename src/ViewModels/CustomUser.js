import ko from 'knockout';

export default class CustomUser {
    constructor(props) {
        this.Id = props.Id;
        this.UserName = props.UserName;
        this.FirstName = props.FirstName;
        this.MiddleName = props.MiddleName;
        this.LastName = props.LastName;
        this.JobTitle = props.JobTitle;
        this.Email = props.Email;
        this.Telephone = props.Telephone;
        this.HireDate = props.HireDate;
        this.isSelected = ko.observable(false);
    }

    sayHello() {
        alert("Hello!");
    }
}
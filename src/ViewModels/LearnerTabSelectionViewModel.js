import ko from 'knockout';
import * as postbox from 'knockout-postbox'
import SearchViewModel from '../ViewModels/SearchViewModel';
import {debounce} from '../Common/Utilities';

export default class LearnerTabSelectionViewModel extends SearchViewModel {
    constructor(props) {
        super(props);
        this.props = props;
        this.scrolled = debounce(this.scrolled, 200);
        this.pageSize = 25;
        this.tabs = {
            learners : {
                serviceEndpoint: 'http://localhost:3000/Users',
                searchText: ko.observable().publishOn("SearchTextModified"),
                page: 1,
                cancelScrollRequest: false                    
            },
            orgs : {
                serviceEndpoint: 'http://localhost:3000/organizations',
                searchText: ko.observable().publishOn("SearchTextModified"),
                page: 1,
                cancelScrollRequest: false
            },
            depts : {
                serviceEndpoint: 'http://localhost:3000/departments',
                searchText: ko.observable().publishOn("SearchTextModified"),
                page: 1,
                cancelScrollRequest: false
            },
            pegroups : {
                serviceEndpoint: 'http://localhost:3000/peoplegroups',
                searchText: ko.observable().publishOn("SearchTextModified"),                  
                page: 1,
                cancelScrollRequest: false
            },
            jobtitles : {
                serviceEndpoint: 'http://localhost:3000/jobtitles',
                searchText: ko.observable().publishOn("SearchTextModified"),                  
                page: 1,
                cancelScrollRequest: false
            },                
            validators : {
            },
            selectedItems: ko.observable({
                learners: ko.observableArray([]),
                orgs:  ko.observableArray([]),
                depts:  ko.observableArray([]),
                pegroups:  ko.observableArray([]),
                jobtitles:  ko.observableArray([]),
                validators:  ko.observableArray([])            
            })                                             
        };

        //Override base results variable
        this.results = {
            learners : ko.observableArray(),
            orgs: ko.observableArray(),
            depts: ko.observableArray(),
            pegroups: ko.observableArray(),
            jobtitles: ko.observableArray()
        };
        this.currentTab = ko.observable("learners");
        this.isLoading = ko.observable(false)        

        //This fires when a change to the search query textbox changes
        this.searchText.subscribe((function(query) {      
            const tabname = this.currentTab();       
            this.tabs[tabname].page = 1; //reset the page when a search is executed
            this.tabs[tabname].cancelScrollRequest = false;
            this.fetchData(query);
        }).bind(this));  

        //Fires when the JSON for the selectedItem changes, then the subscribe is called with the obj which changed
        ko.computed(() => {
            return ko.toJSON(this.tabs.selectedItems); //tickle the subscribe with the object that changed
        }).subscribe( (objItems) => {
            //debugger;
            //console.log("SelectedItems", this.tabs.selectedItems)
        });

        this.init(); //prefetch all data on construction
    }

    init() {
        this.isLoading(true);

        this.fetch({url: this.urlBuilder(this.tabs.learners.serviceEndpoint, "", 1, this.pageSize, null, 'asc')}).done(function(response) {
            let resultData = this.getMappedData(response);
            this.results.learners(resultData);
            this.isLoading(false);
        })
        this.fetch({url: this.urlBuilder(this.tabs.orgs.serviceEndpoint, "", 1, this.pageSize, null, 'asc')}).done(function(response) {
            let resultData = this.getMappedData(response);
            this.results.orgs(resultData);
            this.isLoading(false);
        })            
        this.fetch({url: this.urlBuilder(this.tabs.depts.serviceEndpoint, "", 1, this.pageSize, null, 'asc')}).done(function(response) {
            let resultData = this.getMappedData(response);
            this.results.depts(resultData);
            this.isLoading(false);
        }) 
        this.fetch({url: this.urlBuilder(this.tabs.pegroups.serviceEndpoint, "", 1, this.pageSize, null, 'asc')}).done(function(response) {
            let resultData = this.getMappedData(response);
            this.results.pegroups(resultData);
            this.isLoading(false);
        })
        this.fetch({url: this.urlBuilder(this.tabs.jobtitles.serviceEndpoint, "", 1, this.pageSize, null, 'asc')}).done(function(response) {
            let resultData = this.getMappedData(response);
            this.results.jobtitles(resultData);
            this.isLoading(false);
        })          
    }

    fetchData(query){
        const tabname = this.currentTab();
        this.isLoading(true);
        this.fetch({url: this.urlBuilder(this.tabs[tabname].serviceEndpoint, query, 1, this.pageSize, null, 'asc')}).done((response) => {
            let resultData = this.getMappedData(response);
            this.results[tabname](resultData);
            this.isLoading(false);
        });   
    }

    //Override Parent Implementation to manage tab scroll requests
    scrolled(data, event) {        
        let elem = $(event.target);
        const tabname = this.currentTab();
        if (elem.scrollTop() > (elem[0].scrollHeight - elem[0].offsetHeight - 40)) { 
            this.tabs[tabname].page++
            var ajaxOptions = {
                url: this.urlBuilder(this.tabs[tabname].serviceEndpoint, this.searchText(), this.tabs[tabname].page, this.pageSize, null, 'asc'),
            };    
            if(!this.cancelScrollRequest) {               
                this.fetch(ajaxOptions, true).done( (response) => {
                    if(response.length === 0) {
                        this.tabs[tabname].cancelScrollRequest = true;
                    } else {
                        let resultData = this.getMappedData(response);
                        let results = this.results[tabname]; //get observable array
                        results(results().concat(resultData));                        
                    }
                });
            }
        }   
    }   

    setCurrentTab(data, tabName) {
        this.currentTab(tabName);
    }

    showClearSelections() {
        let selected = this.tabs.selectedItems();
        if(selected.learners().length>0 || 
            selected.orgs().length>0 || 
            selected.depts().length>0 || 
            selected.pegroups().length>0 || 
            selected.jobtitles().length>0) {
                return true;
        } 
        return false;
    }

    handleClearSelections() {
        let selected = this.tabs.selectedItems();
        let verify = confirm("Are you sure you want to clear the current selected items?")
        if(this.showClearSelections() && verify) {
            selected.learners([]);
            selected.orgs([]);
            selected.depts([]);
            selected.pegroups([])
            selected.jobtitles([]);
        }

    }
}